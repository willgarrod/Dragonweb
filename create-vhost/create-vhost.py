#!/usr/bin/python
import os
import sys
import subprocess
import optparse

parser = optparse.OptionParser()
parser.add_option('-d', '--domain', dest='domain', type='string',
    help='Domain name for virtual host.')
parser.add_option('-u', '--username', dest='username',
    help='Username for SFTP access.')
parser.add_option('-p', '--password', dest='password',
    help='password for SFTP.')
parser.add_option('-D', '--dragon', dest='dragon',
default=False, help='Creates parked site for dragonmicro customer')
parser.add_option('-n', '--nosftp', dest='nosftp',
default=False, help='Does not configure sftp')
parser.add_option('-R', '--rtg', dest='rtg',
default=False, help='Creates parked site for rtg domain')
(options, args) = parser.parse_args()

def vhostConfig(domain):
    #Create VirtualHost config file in /etc/httpd/sites-available
    os.chdir('/etc/httpd/sites-available')
    vhost_config = open(domain + '.conf',
        'w')
    vhost_config.write(
'<VirtualHost *:80> \n \
    ServerName www.' + domain + ' \n \
    ServerAlias ' + domain + ' \n \
    DocumentRoot /var/www/' + domain + '/public_html \n \
    ErrorLog /var/www/' + domain + '/error.log \n \
    CustomLog /var/www/' + domain + '/requests.log combined \n \
    php_admin_value sendmail_path "/usr/sbin/sendmail -fadmin@'
    + domain + ' -t -i" \n \
</VirtualHost> \n'
    )
    vhost_config.close

    #Create symlink from sites-available to sites-enabled to enable the
    #VirtualHost
    subprocess.call('ln -s /etc/httpd/sites-available/' + domain +
        '.conf' ' /etc/httpd/sites-enabled/' + domain + '.conf', shell=True)
    return

def createDocumentRoot(domain):
    #Create DocumentRoot in /var/www
    os.chdir('/var/www')
    subprocess.call('mkdir ' + options.domain, shell=True)
    subprocess.call('mkdir ' + options.domain + '/public_html', shell=True)
    return

def sftpUser(username, password, domain):
    #Create user for SFTP access and set password
    subprocess.call('useradd ' + username, shell=True)
    subprocess.call('echo ' + password + '| passwd --stdin ' +
        username , shell=True)

    #Add user to sftp_users group
    subprocess.call('usermod -aG sftp_users ' + username , shell=True)

    #Set users home directory to one level up from DocumentRoot
    subprocess.call('usermod -d' + domain + ' ' + username,
        shell=True)
    return

def setOwn(username, domain):
    os.chdir('/var/www')
    #Set ownership for DocumentRoot
    subprocess.call('chown -R ' + username + ':apache ' +
        domain + '/public_html' ,shell=True)
    subprocess.call('chown root:apache '  + domain ,shell=True)
    return

def setPermissions(domain):
    os.chdir('/var/www')
    subprocess.call('chmod -R 750 ' + domain + '/public_html'
        ,shell=True)
    return

if options.nosftp == 'True':
   vhostConfig(domain = options.domain)
   createDocumentRoot(domain = options.domain)

elif options.dragon == 'True':
    #Create VirtualHost config file in /etc/httpd/sites-available
    vhost_config = open('/etc/httpd/sites-available/' + options.domain +
        '.conf', 'w')
    vhost_config.write(
'<VirtualHost *:80> \n \
    ServerName www.' + options.domain + ' \n \
    ServerAlias ' + options.domain + ' \n \
    DocumentRoot /var/www/dmholding/public_html \n \
	ErrorLog /var/www/dmholding/error.log \n \
    CustomLog /var/www/dmholding/requests.log combined \n \
</VirtualHost> \n'
    )
    vhost_config.close

    #Create symlink from sites-available to sites-enabled to enable the
    #VirtualHost
    subprocess.call('ln -s /etc/httpd/sites-available/' + options.domain +
        '.conf' ' /etc/httpd/sites-enabled/' + options.domain + '.conf',
        shell=True)

    print 'VirtualHost and parked website created for ' + options.domain

elif options.rtg == 'True':
    vhostConfig(domain = options.domain)
    createDocumentRoot(domain = options.domain)
    os.chdir('/var/www/' + options.domain)
    #Copy RTG Parking site files over
    subprocess.call('cp -r /var/www/rtg-parking/* /var/www/' + options.domain +
        '/public_html/' ,shell=True)

    siteIndex = open('/var/www/' + options.domain + '/public_html/index.html',
        'w')

    siteIndex.write('<!DOCTYPE html> \n \
        <html lang="en"> \n \
        <head> \n \
            <meta charset="utf-8"> \n \
            <meta name="viewport" content="width=device-width, initial-scale=1,\
            maximum-scale=1, user-scalable=no"> \n \
            <meta name="description" content=""> \n \
            <meta name="author" content=""> \n \
            <title>' + options.domain + '</title> \n \
            <!-- Bootstrap Core CSS --> \n \
            <link href="assets/css/bootstrap/bootstrap.min.css" \
            rel="stylesheet" type="text/css"> \n \
            <link href="assets/css/holding.css" rel="stylesheet"> \n \
        </head> \n \
        <body id="page-top"> \n \
            <div id="wrapper"> \n \
                <header class="intro-img intro-dark-bg"\
                style="background-image: url(assets/img/demo-bgs/\
                demo-bg-1.jpg)" data-stellar-background-ratio="0.5"> \n \
                    <div class="overlay"></div> \n \
                    <div class="intro-body" data-scrollreveal="move 0 over \
                    1.5s"> \n \
                        <div class="container"> \n \
                            <img src="assets/img/rtg.png" \
                            class="intro-logo"> \n \
                            <hr class="primary"> \n \
                            <h4><span class="text-primary">' + options.domain \
                            +'</span> is currently in development.</h4> \n \
                            <h5>For information about Radio Technology Group \
                            please see our <a href="http://www.radiotechnology\
                            group.com/">website</a> or contact us on <a href="t\
                            el:0870 600 2006">0870 600 2006</a></h5> \n \
                        </div> \n \
                    </div> \n \
              </header>\n \
            </div> \n \
        </body> \n \
        </html>')
    siteIndex.close

    setOwn(username = "rtg", domain = options.domain)
    setPermissions(domain = options.domain)

    print 'VirtualHost and parked website created for ' + options.domain

else:
    vhostConfig(domain = options.domain)
    createDocumentRoot(domain = options.domain)
    sftpUser(username = options.username, password = options.password,
        domain = options.domain)
    setOwn(username = options.username, domain = options.domain)
    setPermissions(domain = options.domain)

    print 'VirtualHost created for ' + options.domain
