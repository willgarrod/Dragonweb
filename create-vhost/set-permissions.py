#!/usr/bin/python
import os
import sys
import subprocess
import optparse

parser = optparse.OptionParser()
parser.add_option('-d', '--domain', dest='domain', type='string',
    help='Domain name of virtual host.')
parser.add_option('-u', '--username', dest='username',
    help='Username of user account used for SFTP access.')
(options, args) = parser.parse_args()

os.chdir('/var/www/')

#set selinux lables for the virtual host
subprocess.call('chcon -R -t httpd_sys_rw_content_t ' + options.domain, shell=True)

#restart httpd
subprocess.call('systemctl restart httpd', shell=True)

#set ownership, permmisions and selinux lables for log files
subprocess.call('chcon -R -t httpd_log_t ' + options.domain + '/error.log', shell=True)
subprocess.call('chown -R apache:apache ' + options.domain + '/error.log', shell=True)
subprocess.call('chmod -R 600 ' + options.domain + '/error.log', shell=True)
subprocess.call('chcon -R -t httpd_log_t ' + options.domain + '/requests.log', shell=True)
subprocess.call('chown -R apache:apache ' + options.domain + '/requests.log', shell=True)
subprocess.call('chmod -R 600 ' + options.domain + '/requests.log', shell=True)

#set ownership, permmisions and selinux lables for document root
subprocess.call('chcon -R -t httpd_sys_content_t ' + options.domain + '/public_html/', shell=True)
subprocess.call('chown -R ' + options.username + ':apache ' + options.domain + '/public_html/', shell=True)
subprocess.call('chmod 755 ' + options.domain + '/public_html/', shell=True)
